# gpg-cheat-sheet

More complete guide here, thanks https://devhints.io/gnupg

## Version used

gpg (GnuPG) 2.2.12

## Key pair creation

```bash
gpg --full-gen-key
```
* Key type : RSA (option 1)
* Size : 4096
* Expiration : Adapt this response to your needs
* Name : johndoggett
* Email : johndoggett@foo.bar
* Comment : 
* Validation
* Passphrase

## Key management
### List public keys
```bash
gpg -k
gpg --list-keys
```
### List secret keys
```bash
gpg -K
gpg --list-secret-keys
```
### Export public keys
```bash
gpg -o /path/to/keyname.asc --armor --export <keyname>
```

### Export private keys
```bash
gpg -o /path/to/keyname.key --armor --export-secret-keys <keyname> 
```

### Import keys
```bash
gpg --import /path/to/keyname.key
gpg --import /path/to/keyname.asc
```

## Encryption/ Decryption
### Encrypt
```bash
gpg --output /path/to/secret.txt.gpg --encrypt --recipient <recipient_public_key> /path/to/secret.txt
```

### Decrypt
```bash
gpg --output /path/to/secret.txt -d /path/to/secret.txt.gpg
```

## Create a file signature
Based on this article, thanks
https://yanhan.github.io/posts/2017-09-27-how-to-use-gpg-to-encrypt-stuff.html
```bash
# Make SHA256
shasum -a 256 /path/to/secret.txt | awk '{print $1}' >/path/to/secret.txt.sha256sum
# Create signature with password prompt 
gpg --sign --default-key ${<sender_private_key>} \
    --recipient ${<recipient_public_key>} \
    --output /path/to/secret.txt.sha256sum.sig /path/to/secret.txt.sha256sum
# Create signature without password prompt 
gpg --pinentry-mode loopback --passphrase="mySuperSecretPassphrase" \
    --sign --default-key ${<sender_private_key>} \
    --recipient ${<recipient_public_key>} \
    --output /path/to/secret.txt.sha256sum.sig /path/to/secret.txt.sha256sum
```